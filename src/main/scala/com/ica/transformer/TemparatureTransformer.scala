package com.ica.transformer

import com.ica.DataProcessor.getClass
import com.ica.common.ICASpark
import org.apache.log4j.Logger
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.types.{FloatType, StringType, StructType}

/**
 * Transformer class to convert raw data into parquet format with append mode
 */
object TemparatureTransformer  extends ICASpark {

  val log = Logger.getLogger(getClass.getName)

  /**
   *  This method  takes preprocessed data and read it as dataframe and store into parquet format with append mode by year partitions
   * @param spark  spaarksession
   * @param schema generic schema to read all input files
   * @param stageOutPutLoc  preprocessedoutput location
   * @param finalOutPut final output location
   */
   def finalTableCreation(spark: SparkSession, schema: StructType, stageOutPutLoc: String,finalOutPut: String) = {
    log.info("Started processing final data")
     val df_with_schema = spark.read.format("csv")
      .schema(schema)
      .load(stageOutPutLoc)
     df_with_schema.printSchema()
     df_with_schema.write.mode(SaveMode.Append).partitionBy("year").parquet(finalOutPut)
     df_with_schema.createTempView("Temparature_Data")

     //here we can do selects and any queryies on processed table
     sqlContext.sql("select * from Temparature_Data").show()

   }

  /**
   * This function normalize raw files and convert multispaces delimeter into csv file
   * @param spark sparksession
   * @param stageOutPutLoc location to store one/many spaces delimiter deta to csv,
   * @param inPutLocation  input location of source files
   */
   def preProcessData(spark: SparkSession, stageOutPutLoc: String, inPutLocation: String) = {
     log.info("Started preprocessing")
     val rddFromFile = spark.sparkContext.textFile(inPutLocation)
     val newRDD1 = rddFromFile.map(elt => elt.trim).map(elt => elt.replaceAll("""[\t\p{Zs}]+""", ","))
     newRDD1.coalesce(1).saveAsTextFile(stageOutPutLoc)
   }

  /**
   *  Schema of the table
   */
   def getSchema = {
    val schema = new StructType()
      .add("year", StringType)
      .add("month", StringType)
      .add("dat", StringType)
      .add("m_temp", FloatType, true)
      .add("n_temp", FloatType, true)
      .add("e_temp", FloatType, true)
      .add("max_temp", FloatType, true)
      .add("min_temp", FloatType, true)
      .add("mean_temp", FloatType, true)
    schema
  }
}
