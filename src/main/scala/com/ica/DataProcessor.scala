package com.ica

import com.ica.transformer.TemparatureTransformer.{finalTableCreation, getSchema, preProcessData, spark}
import org.apache.log4j.Logger
import org.apache.spark.sql.types.StructType

object DataProcessor {
  /**
   *  Driver class to run etl job for the given input locations through args
   * @param args
   */
  def main(args: Array[String]): Unit = {
    val log = Logger.getLogger(getClass.getName)
    if(args.length<3){
      log.info("Required Arguments  stage_location,input_location,output_location")
      return 0;
    }
    val stageOutPutLoc = args(0)
    val inPutLocation = args(1)
    val factOutPutLoc = args(2)
    val schema: StructType = getSchema


    try {
      
      //Function to normalize the input data
      preProcessData(spark, stageOutPutLoc, inPutLocation)
      // Function to create final data
      finalTableCreation(spark, schema, stageOutPutLoc, factOutPutLoc)

    }catch {
      case e: Exception => log.error("Failed to execute DataProcessor job")
        e.printStackTrace()
        System.exit(1)
    }

  }


}
