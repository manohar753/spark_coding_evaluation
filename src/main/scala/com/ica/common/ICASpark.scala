package com.ica.common

import org.apache.spark.sql.SparkSession

/**
 * sparksession and sqlcontext ceated and used in the project
 */
trait ICASpark {
  val spark = SparkSession.builder
    .master("local[*]")
    .appName("ICA-TEST")
    .getOrCreate()



  val sqlContext = spark.sqlContext

}
