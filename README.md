# Spark Coding Evaluation
This usecase uses set of files that contain raw daily air 
temperature observation data from the Stockholm Old Astronomical Observatory
(59.35N, 18.05E) 1756-2017.

The original data is cleaned,ingested and transformed to use downstream systems.

## Prerequisites

Following sotwares are required to setup project locally
 - Intelij Latest Versi
 - Spark 3.1.1
 - Scala 2.12
 
## Source Files
https://bolin.su.se/data/stockholm-thematic/raw_individual_temperature_observations.php
## Author
Manohar
